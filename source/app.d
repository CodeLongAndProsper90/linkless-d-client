import std.stdio;
import std.net.curl: get;
import std.getopt;
import std.json;
import std.format;
import std.string;

void main(string[] Args)
{
  bool decode = false;
  bool del = false;
  string user = "system";
  auto args = getopt(
                     Args,
                     "decode|d", "Decode the url from the given [name]", &decode,
                     "delete", "Delete the symbolic link at [name]", &del,
					 "as", "The user to add the url as", &user,
);
  if (args.helpWanted)
    {
      defaultGetoptPrinter("Usage: linkless [OPTIONS] name url [TOKEN]\n(url not required if using -d)",args.options);
      return;
  }
  const string api = "https://lesslink.codelongandpros.repl.co/";
  if (!decode && !del) {
    if (Args.length < 3) {

          defaultGetoptPrinter("Usage: linkless [OPTIONS] name url [TOKEN]",args.options);
          return;
    }
    const string name = Args[1];
    const string url = Args[2];
    if (user == "system" ) {
      writeln("You may want to supply an [OWNER] for this link. Else, the `system` owner will be used.");
      writeln("This means anyone can delete it!");
      writeln("Enter `system, yes please` without the backticks to continue");
      if (chomp(readln()) != "system, yes please")  {
          writeln("Invalid input");
          return;
        }
        user = "system";
    } 
    auto  resp = parseJSON(get(format("%sadd?name=%s&url=%s&owner=%s", api, name, url, user)));
    long status = resp["status"].integer;
    if (status != 0) {
      writeln("Error!");
      writefln("API failed with the error code %d\nReason: %s", status, resp["reason"].str);
    }
  }
  else if (decode) {
    /*
    if (Args.length < 2) {
      defaultGetoptPrinter("Usage: linkless [OPTIONS] name url [TOKEN]",args.options);
    }
    */
    const string name = Args[1];
    auto resp = parseJSON(get(format("%sget?name=%s", api, name)));
    debug writeln(resp["url"].str);
    string url = resp["url"].str;
    if (url == "") {
      url = "\033[31m/dev/null\033[0m";
    }
    writefln("%s -> %s",name, url);
    
  }
  else if (del) {
    const string name = Args[1];
    
    auto resp = parseJSON(get(format("%s/del?name=%s&owner=%s", api, name, user)));
    if (resp["error"].integer != 0) {
		writefln("Server resonded with error %d (unknown reason)", resp["error"].integer);
	}
  }
  
}
